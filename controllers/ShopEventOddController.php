<?php

namespace frontend\controllers;

use Yii;
use app\models\ShopEventOdd;
use app\models\ShopEventOddSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShopEventOddController implements the CRUD actions for ShopEventOdd model.
 */
class ShopEventOddController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopEventOdd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopEventOddSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopEventOdd model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopEventOdd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopEventOdd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->event_odd_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ShopEventOdd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->event_odd_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ShopEventOdd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopEventOdd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopEventOdd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopEventOdd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddeventtoshop() {
        $model = new ShopEventOdd();

        if(Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            
            $parent_match_id = $model->parent_match_id;

            $homeOdd = $model->homeOdd;
            $drawOdd = $model->drawOdd;
            $awayOdd = $model->awayOdd;

            $dataKey = $homeOdd.",".$drawOdd.",".$awayOdd;

            $data = rtrim($dataKey,' ');            
            
            $result = $model->addEventToShop($parent_match_id, $data);

            if($result) {
                $msg = "Event added to shop successfully.";
                $error = 'success';

                Yii::$app->getSession()->setFlash($error, $msg);

                return $this->redirect(['index']);

            } else {
                return $this->redirect('index');
            }
            

        } else {
        return $this->render('add_event_to_shop', [
                'model' => $model,
            ]);
        }
    }


}
