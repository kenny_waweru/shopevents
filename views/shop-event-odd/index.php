<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShopEventOddSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shop Event Odds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-event-odd-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
         <a class="btn btn-info" href="<?php echo Url::to(['shop-event-odd/addeventtoshop']) ?>">Add Events To Shop</a>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'event_odd_id',
            'parent_match_id',
            'sub_type_id',
            //'max_bet',
            'created',
            'modified',
            'odd_key',
            'odd_value',
            //'odd_alias',
            //'special_bet_value',
            //'id_of_player',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
