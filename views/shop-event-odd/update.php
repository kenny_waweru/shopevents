<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShopEventOdd */

$this->title = 'Update Shop Event Odd: ' . $model->event_odd_id;
$this->params['breadcrumbs'][] = ['label' => 'Shop Event Odds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_odd_id, 'url' => ['view', 'id' => $model->event_odd_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shop-event-odd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
