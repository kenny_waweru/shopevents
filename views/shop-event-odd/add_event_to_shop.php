<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\UserBetCancel */

$this->title = 'Shop Event Odds';
$this->params['breadcrumbs'][] = ['label' => 'Shop Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop_event_odd">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create_shop_event_odd_form', [
        'model' => $model,
    ]) ?>

</div>
	