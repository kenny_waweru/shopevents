-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2017 at 11:54 AM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 5.6.29-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `betika`
--

-- --------------------------------------------------------

--
-- Table structure for table `shop_event_odd`
--

CREATE TABLE `shop_event_odd` (
  `event_odd_id` int(11) NOT NULL,
  `parent_match_id` int(11) NOT NULL,
  `sub_type_id` int(11) NOT NULL,
  `max_bet` decimal(10,0) NOT NULL,
  `odd_key` varchar(200) NOT NULL,
  `odd_value` varchar(20) NOT NULL,
  `odd_alias` varchar(20) NOT NULL,
  `special_bet_value` varchar(20) NOT NULL,
  `id_of_player` varchar(30) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_event_odd`
--

INSERT INTO `shop_event_odd` (`event_odd_id`, `parent_match_id`, `sub_type_id`, `max_bet`, `odd_key`, `odd_value`, `odd_alias`, `special_bet_value`, `id_of_player`, `status`, `created`, `modified`) VALUES
(13, 11051995, 10, '1', '1', '1.24', 'NULL', '', 'NULL', 0, '2017-05-23 12:05:33', '2017-05-23 12:05:33'),
(14, 11051995, 10, '1', 'X', '3.89', 'NULL', '', 'NULL', 0, '2017-05-23 12:05:33', '2017-05-23 12:05:33'),
(15, 11051995, 10, '1', '2', '5.6', 'NULL', '', 'NULL', 0, '2017-05-23 12:05:33', '2017-05-23 12:05:33'),
(16, 12051996, 10, '1', '1', '1.45', 'NULL', '', 'NULL', 0, '2017-05-23 12:21:48', '2017-05-23 12:21:48'),
(17, 12051996, 10, '1', 'X', '3.89', 'NULL', '', 'NULL', 0, '2017-05-23 12:21:48', '2017-05-23 12:21:48'),
(18, 12051996, 10, '1', '2', '6.0', 'NULL', '', 'NULL', 0, '2017-05-23 12:21:48', '2017-05-23 12:21:48'),
(19, 12051996, 10, '1', '1', '3.8', 'NULL', '', 'NULL', 0, '2017-05-23 12:23:16', '2017-05-23 12:23:16'),
(20, 12051996, 10, '1', 'X', '6.8', 'NULL', '', 'NULL', 0, '2017-05-23 12:23:16', '2017-05-23 12:23:16'),
(21, 12051996, 10, '1', '2', '3.8', 'NULL', '', 'NULL', 0, '2017-05-23 12:23:16', '2017-05-23 12:23:16'),
(22, 12051996, 10, '1', '1', '', 'NULL', '', 'NULL', 0, '2017-05-23 13:29:40', '2017-05-23 13:29:40'),
(23, 12051996, 10, '1', 'X', '', 'NULL', '', 'NULL', 0, '2017-05-23 13:29:40', '2017-05-23 13:29:40'),
(24, 12051996, 10, '1', '2', '', 'NULL', '', 'NULL', 0, '2017-05-23 13:29:40', '2017-05-23 13:29:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shop_event_odd`
--
ALTER TABLE `shop_event_odd`
  ADD PRIMARY KEY (`event_odd_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shop_event_odd`
--
ALTER TABLE `shop_event_odd`
  MODIFY `event_odd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;