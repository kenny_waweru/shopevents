<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shop_event_odd".
 *
 * @property integer $event_odd_id
 * @property integer $parent_match_id
 * @property integer $sub_type_id
 * @property string $max_bet
 * @property string $created
 * @property string $modified
 * @property string $odd_key
 * @property string $odd_value
 * @property string $odd_alias
 * @property string $special_bet_value
 * @property string $id_of_player
 * @property integer $status
 */
class ShopEventOdd extends \yii\db\ActiveRecord
{

    public $homeOdd;
    public $drawOdd;
    public $awayOdd;
    /**
     * @inheritdoc
     */


    public static function tableName()
    {
        return 'shop_event_odd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_match_id', 'sub_type_id', 'max_bet', 'created', 'odd_key', 'odd_value', 'odd_alias', 'special_bet_value', 'id_of_player', 'status', 'homeOdd', 'drawOdd', 'awayOdd'], 'required'],
            [['parent_match_id', 'sub_type_id', 'status'], 'integer'],
            [['max_bet'], 'number'],
            [['created', 'modified'], 'safe'],
            [['odd_key'], 'string', 'max' => 200],
            [['odd_value', 'odd_alias', 'special_bet_value'], 'string', 'max' => 20],
            [['id_of_player'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_odd_id' => 'Event Odd ID',
            'parent_match_id' => 'Parent Match ID',
            'sub_type_id' => 'Sub Type ID',
            'max_bet' => 'Max Bet',
            'created' => 'Created',
            'modified' => 'Modified',
            'odd_key' => 'Odds',
            'odd_value' => 'Odd Value',
            'odd_alias' => 'Odd Alias',
            'special_bet_value' => 'Special Bet Value',
            'id_of_player' => 'Id Of Player',
            'status' => 'Status',
        ];
    }

    public function match_to_add() {
        $data = array();
        $rows = (new \yii\db\Query())
                ->select(["CONCAT(parent_match_id, ' - ' ,home_team, ' - ', away_team) AS _match", 'parent_match_id'])
                ->from('match')
                ->where(['status'=>'1'])
                ->all();
        foreach ($rows as $key => $value) {
            $data[$value['parent_match_id']] = $value['_match'];
        }
        return $data;
    }

    public function addEventToShop($parent_match_id, $data) {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try{
            // constants
            $sub_type_id = 10;
            $max_bet = 1;
            $created = date('Y-m-d H:i:s');
            $odd_alias = 'NULL';
            $special_bet_value = NULL;
            $id_of_player = 'NULL';
            $status = 0;

            // explode comma seperated odds
            $data1 = explode(',', $data);
            //print_r($data1);die();
            // get odd value for each odd_key
            $homeOddValue = $data1[0];
            $drawOddValue = $data1[1];
            $awayOddValue = $data1[2];

            $odds = array(
                "1" => $homeOddValue,
                "X" => $drawOddValue,
                "2" => $awayOddValue

            );
            
            foreach($odds as $key => $value){
                
                $sql = "INSERT INTO shop_event_odd(parent_match_id,sub_type_id,max_bet,odd_key,odd_value,odd_alias, special_bet_value, id_of_player, status, created) 
                        VALUES('$parent_match_id','$sub_type_id','$max_bet','$key','$value','$odd_alias','$special_bet_value','$id_of_player','$status','$created')

                ";

                $connection->createCommand($sql)->execute();
            }

            $transaction->commit();
            return TRUE;
        } catch (Exception $exc) {
            $transaction->rollback();
            return FALSE;
        }
        
    }


}
